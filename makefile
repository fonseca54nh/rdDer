OBJS = src/main.cpp src/keyReader.cpp

CC = g++

COMPILER_FLAGS = -O3 -m32

DEBUGER_FLAGS = -O0 -g -ggdb

LINKER_FLAGS = -lcrypto

OBJECT_NAME = bin/rdDer

DEBUG_NAME = debug/rdDer 

BINDIR=/bin
MANDIR=/usr/share/man
INCLUDEDIR=/usr/include

release: $(OBJS)
	   $(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJECT_NAME)

debug: $(OBJS)
	   $(CC) $(OBJS) $(DEBUGER_FLAGS) $(LINKER_FLAGS) -o $(DEBUG_NAME)

install:
	mkdir -p ${DESTDIR}/${SBINDIR}
	install -m0755 ${OBJECT_NAME} ${BINDIR}
