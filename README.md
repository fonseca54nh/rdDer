# rdDer - Read and Display X509 Info

## About

Command-line tool to read and display x509 certifcates subject and issuers.

## Installation

To intall run the lines below in a terminal.

```
git clone https://gitlab.com/fonseca54nh/rdDer
cd rdDer
sudo make install
```

## Usage

```
rdDer $args
```

![Basic Usage]( Images/usage.png )

## Test 

This repo includes a script called test, wich automates testing. It uses some .der files located in the certifcates directory.

To use it:

```
./test
```

## Yocto 

This repo also ships an yocto generated distro as an iso. To run it simply type the following in a terminal:

```
cd iso/
qemu-img create q -f qcow2 poky.img 10G
qemu-system-x86_64 -hda poky.img -boot d -cdrom core-image-minimal-dev-qemux86.iso
```

### rdDer inside the ISO

Some certificates were also included into this iso under /home/root/certificates. For testing purposes, one can run:

```
rdDer certificates/a.der
```

### meta-rdder

This repo also includes a recipe folder for future yocto builds.
