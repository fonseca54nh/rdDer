LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = "https://gitlab.com/fonseca54nh/rdDer \
	   file://certificates/999b-rsa-example-cert.der \
	   file://certificates/a.der"

S = "${WORKDIR}/git"

do_configure () {
	# Specify any needed configure commands here
	:
}

do_compile () {
	# You will almost certainly need to add additional arguments here
	oe_runmake
}

do_install () {
	install -d ${D}${bindir}
	install /home/topcat/Desktop/rdDer/bin/rdDer ${D}/${bindir}/
	install -d ${D}${base_prefix}/home/root/certificates/
	install -m 0755 ${WORKDIR}/certificates/* ${D}/${base_prefix}/home/root/certificates/
}

FILES_${PN} += " ${bindir}/rdDer \
		 ${base_prefix}/home/root/certificates/* "
RDEPENDS_${PN} += " openssl libgcc "
