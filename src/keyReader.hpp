#include "header.hpp"

// reader class
class reader
{
protected:
	X509* certificate = NULL;
	X509_NAME* subject      = NULL;

	BIO *bio_out = BIO_new_fp( stdout, BIO_NOCLOSE );

	FILE* fp = NULL;
	char buffer[10000];
public:
	void initOpenssl();
	void readFile( const char* );
	int  sigVerify();
	void certInfo();
	void Free();

};
