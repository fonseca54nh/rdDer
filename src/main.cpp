#include "keyReader.hpp"

int main( int argc, char** argv )
{
	// Create class object
	reader a;

	// Init OpenSSL
	a.initOpenssl();
	
	// Read file from stream
	a.readFile( argv[1] );

	// Open the file with OpenSSL
	a.sigVerify();

	// Print subject and issuer
	a.certInfo();

	// Free memory
	a.Free();
}
