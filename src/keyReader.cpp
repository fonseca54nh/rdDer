#include "keyReader.hpp"

// Function to init OpenSSL.
void reader::initOpenssl()
{
	OpenSSL_add_all_algorithms();
	OpenSSL_add_all_ciphers();
	OpenSSL_add_all_digests();
}

//Function that opens a file and stores it into fp.
void reader::readFile( const char* filename )
{
	fp = fopen( filename, "rb" );

	fp == NULL ? std::cout << "========== Error opening file ==========" << std::endl  :
		     std::cout << "========== Success opening file ==========" << std::endl << std::endl;
}

//Function that opens the certificate with d2i_X509_fp, and returs 1 if successfull.
int reader::sigVerify()
{

	certificate = d2i_X509_fp( fp, NULL );

	if( certificate == NULL )
	{
		std::cout << "========== Error reading certificate ==========" << std::endl;
		return 0;
	}
	else
	{

		std::cout << "========== Success reading certificate ==========" << std::endl << std::endl;
		return 1;
	}
}

//Function that prints the subject and Issuer of the certificate.
void reader::certInfo()
{
	subject = X509_get_subject_name( certificate );

	BIO_printf(bio_out,"========== Subject ==========\n");
	printf( "%s", X509_NAME_oneline( subject, buffer, sizeof( buffer ) ) );
	BIO_printf(bio_out,"\n");
	
	BIO_printf(bio_out,"\n========== Issuer ==========\n");
	X509_NAME_print( bio_out, X509_get_issuer_name( certificate ), 0);
	BIO_printf(bio_out,"\n");
}

//Function that frees used memory.
void reader::Free()
{
	BIO_free( bio_out );
	X509_free( certificate );
}
